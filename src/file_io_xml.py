import xml.etree.ElementTree as ET

def read_xml_file(xml_file_path):
    try:
        # Parse the XML file
        tree = ET.parse(xml_file_path)

        # Get the root element
        root = tree.getroot()

        # Access and print specific elements
        for person in root.findall('person'):
            name = person.find('name').text
            age = person.find('age').text
            print(f"Name: {name}, Age: {age}")
    except FileNotFoundError:
        print(f"File not found: {xml_file_path}")
    except ET.ParseError as e:
        print(f"Error parsing the XML file: {e}")


# Specify the path to your XML file
xml_file_path = input("Enter xml file path: ")
read_xml_file(xml_file_path)