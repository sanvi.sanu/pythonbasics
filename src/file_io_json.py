import json

# Read a json file with below schema:
# {
#    "Role": 1,
#    "Name": "My Name",
#    "Grade": 3
# }
def read_json_file(file_name):
    try:
        # Open the JSON file
        with open(file_name, 'r') as json_file:
            # Load the JSON data
            data = json.load(json_file)

            # Access and print specific fields
            roll = data.get('Roll', '')
            name = data.get('Name', '')
            grade = data.get('Grade', '')

            # Print or process the data
            print(f"Roll: {roll}, Name: {name}, Grade: {grade}")
    except FileNotFoundError:
        print(f"File not found: {file_name}")
    except IOError as e:
        print(f"Error reading the JSON file: {e}")
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON: {e}")

# Display Menu
json_file_path = input("Enter json file path: ")
read_json_file(json_file_path)