# Addition lambda
add = lambda x, y: x + y
print(add(3, 5))

# Subtraction lambda
subtract = lambda x, y: x - y
print(subtract(3, 5))

